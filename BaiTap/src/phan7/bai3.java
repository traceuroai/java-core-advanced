package phan7;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

/* Sắp xếp mảng số nguyên 1 chiều gồm n phần tử 
 * cho trước được đọc ra từ file input.txt 
 * bằng phương pháp quick sort, giả thiết các số 
 * nguyên cách nhau bằng dấu cách hoặc xuống dòng*/

public class bai3 {
	
	public static int arr[];

	public static void main(String[] args) throws FileNotFoundException {
		
		File file = new File("C:/Users/trace/Desktop/xtel/BaiTap/input.txt");
		Scanner sc = new Scanner(file, "UTF-8");
		String temp = sc.nextLine();
		sc.close();
		String [] item = temp.split(" ");
		arr = new int [item.length];
		for(int i = 0; i < item.length; i++)
		{
			try {
				arr[i] = Integer.parseInt(item[i]);
			} catch (NumberFormatException e) {
				System.out.println("Chứa ký tự không phải số, không thể sắp xếp");
				System.exit(0);
			}
		}
		System.out.println("Mang ban dau la : ");
		printArr(arr);
		
		Arrays.sort(arr);// Arrays.sort đã cài đặt thuật toán quicksort
		System.out.println("\nMang sau khi sap xep la :");
		printArr(arr);
		
	}
	
	public static void printArr(int [] arr)
	{
		for(int i = 0; i < arr.length; i++)
		{
			System.out.print(arr[i] + " ");
		}
	}
	
}
