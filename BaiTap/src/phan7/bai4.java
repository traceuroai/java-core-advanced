package phan7;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class bai4 {

	static String DRIVER_NAME = "com.mysql.jdbc.Driver";
	static String DB_URL = "jdbc:mysql://localhost:3306/demo";
	static String USER_NAME = "root";
	static String PASSWORD = "baoai";

	public static void main(String[] args) {

		String str;
		int count;
		Scanner sc = new Scanner(System.in);
		System.out.println("Nhập số lượng sinh viên : ");
		str = sc.nextLine();
		if (str.trim().equals("")) {
			System.out.println("Bạn nhập chuỗi rỗng ");
			System.exit(0);
		}

		try {
			count = Integer.parseInt(str);
			if (count <= 0) {
				System.out.println("Số lượng không hợp lệ");
				System.exit(0);
			}

			// đối tượng list lưu thông tin danh sách sinh viên
			List<SinhVien> list = new ArrayList<>();
			for (int i = 0; i < count; i++) {
				String name;
				int age;
				System.out.println("nhập thông tin sinh viên " + i + " : ");
				System.out.println("tên : ");
				name = sc.nextLine();

				// kiểm tra xem tên đã tồn tại chưa
				for (SinhVien svTemp : list) {
					while (name.equals(svTemp.getName())) {
						System.out.println("Tên sinh viên đã tồn tại, vui lòng nhập tên khác");
						name = sc.nextLine();
					}
				}

				System.out.println("tuổi : ");
				age = sc.nextInt();
				sc.nextLine();
				SinhVien sv = new SinhVien(name, age);
				list.add(sv);

			}

			insert(list);

		} catch (NumberFormatException e) {
			System.out.println("Số lương nhập vào không hợp lệ");
			e.printStackTrace();
		}

	}

	public static Connection getConnection() {
		Connection con = null;
		try {
			Class.forName(DRIVER_NAME);
			con = DriverManager.getConnection(DB_URL, USER_NAME, PASSWORD);
			System.out.println("connect successfully!");
		} catch (Exception e) {
			System.out.println("connect failure!");
			e.printStackTrace();
		}

		return con;
	}

	public static void insert(List<SinhVien> list) {
		Connection connection = null;
		Statement stmt = null;
		try {
			connection = getConnection();
			for (int i = 0; i < list.size(); i++) {
				String sql = "INSERT INTO `demo`.`person` (`name`, `age`) VALUES ('" + list.get(i).getName() + "', '"
						+ list.get(i).getAge() + "')";
				stmt = connection.createStatement();
//					sttm = connection.prepareStatement(sql);
				stmt.executeUpdate(sql);
			}
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
