package phan7;

import java.util.Scanner;

/* Viết chương trình nhập 1 số cho tới khi
 * nhập đúng số cần nhập thì hiện thông báo thành công. 
 * Số lần nhập tối đa 5 lần nếu nhập sai thì sẽ dừng chương trình 
 * và hiện thông báo nhập lỗi.*/ 

public class bai1 {
	
	public static void main(String[] args) {
		
		int i, a = 8, j = 0;
		String str;
		Scanner sc = new Scanner(System.in);
		System.out.println("Số cần nhập : " + a);
		do {
			System.out.println("nhập vào số : ");
			str = sc.nextLine();
			if(str.trim().equals(""))
			{
				System.out.println("Bạn nhập chuỗi rỗng, vui lòng nhập lại !");
				continue;
			}
			try {
				i = Integer.parseInt(str);
				if(i == a)
				{
					System.out.println("Thành công");
					return;
				}
			} catch (NumberFormatException e) {
				System.out.println("Bạn nhập chuỗi không hợp lệ, vui lòng chỉ nhập số !");
			}
			j++;
		} while (j < 5);
		
		System.out.println("Lỗi nhập không chính xác");
		
	}

}
