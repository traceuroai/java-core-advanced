package phan7;

import java.util.Scanner;

// Viết chương trình tính tiền điện, 
// cho phép nhập số điện đã dùng trong tháng, 
// giả thiết 100 số đầu tính giá 1000đ/1 số, 
// kể từ 100 số tính giá 1500đ/số, kể từ 150 số tính giá 2000đ/số.

public class bai2 {

	public static void main(String[] args) {
		int soDien;
		String str;
		float sum = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("nhập số điện đã dùng trong tháng");
		str = sc.nextLine();
		if(str.trim().equals(""))
		{
			System.out.println("Bạn đã nhập chuỗi rỗng !");
			System.exit(0);
		}
		try {
			soDien = Integer.parseInt(str);
			if(soDien < 0)
			{
				System.out.println("Số bạn nhập không hợp lệ ");
				System.exit(0);
			}
			else if(soDien > 0 && soDien <= 100)
			{
				sum = soDien * 1000;
			}
			else if(soDien > 100 && soDien <= 150)
			{
				sum = (soDien - (soDien % 100)) * 1000 + (soDien % 100) * 1500;
			}else 
			{
				sum = 100 * 1000 + 50 * 1500  + (soDien - 150) * 2000;
			}
			System.out.println("Số tiền phải trả là : " + sum);
		} catch (NumberFormatException e) {
			System.out.println("Bạn đã nhập chuỗi không hợp lệ, xin vui lòng nhập số !");
		}
		
	}
	
}
