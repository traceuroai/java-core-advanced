package vdSocket;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketTCPIPServer {

	public static void main(String[] args) {
		ServerSocket listener = null;
		String line;
		BufferedReader is;
		BufferedWriter os;
		Socket socketOfServer = null;
		
		
		try {
	           listener = new ServerSocket(9999);
	       } catch (IOException e) {
	           System.out.println(e);
	           System.exit(1);
	       }
		
		try {
	           System.out.println("Server is waiting to accept user...");
	 
	 
	           // Chấp nhận một yêu cầu kết nối từ phía Client.
	           // �?ồng th�?i nhận được một đối tượng Socket tại server.
	 
	           socketOfServer = listener.accept();
	           System.out.println("Accept a client!");
	 
	      
	           // Mở luồng vào ra trên Socket tại Server.
	           is = new BufferedReader(new InputStreamReader(socketOfServer.getInputStream()));
	           os = new BufferedWriter(new OutputStreamWriter(socketOfServer.getOutputStream()));
	 
	    
	           // Nhận được dữ liệu từ ngư�?i dùng và gửi lại trả l�?i.
	           while (true) {
	               // �?�?c dữ liệu tới server (Do client gửi tới).
	               line = is.readLine();
	 
	               // Ghi vào luồng đầu ra của Socket tại Server.
	               // (Nghĩa là gửi tới Client).
	               os.write(">> " + line);
	               // Kết thúc dòng
	               os.newLine();
	               // �?ẩy dữ liệu đi
	               os.flush();  
	 
	               // Nếu ngư�?i dùng gửi tới QUIT (Muốn kết thúc trò chuyện).
	               if (line.equals("QUIT")) {
	                   os.write(">> OK");
	                   os.newLine();
	                   os.flush();
	                   break;
	               }
	           }
	 
	       } catch (IOException e) {
	           System.out.println(e);
	           e.printStackTrace();
	       }
	       System.out.println("Sever stopped!");
	   
		
	}

}
